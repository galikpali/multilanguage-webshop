#!/usr/bin/env bash

until nc -z -v -w30 ${DB_HOST} ${DB_PORT}
do
  echo "Waiting for database connection..."
  sleep 5
done

php /var/www/html/artisan migrate
php /var/www/html/artisan db:seed