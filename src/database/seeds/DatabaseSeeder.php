<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    public function run(): void
    {
        DB::table('languages')->insertOrIgnore([
            'code' => getenv('DEFAULT_LANGUAGE_CODE')
        ]);
    }
}
