<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    public const TABLE_NAME = 'products';

    public const COLUMN_ID = 'id';

    public function up(): void
    {
        Schema::create(self::TABLE_NAME, function (Blueprint $table): void {
            $table->id(self::COLUMN_ID);
            $table->foreignId('title_translation_id')
                ->references(CreateTranslationsTable::COLUMN_ID)
                ->on(CreateTranslationsTable::TABLE_NAME);
            $table->foreignId('html_description_translation_id')
                ->references(CreateTranslationsTable::COLUMN_ID)
                ->on(CreateTranslationsTable::TABLE_NAME);
            $table->integer('price')->unsigned();
            $table->integer('published_from')->unsigned();
            $table->integer('published_to')->unsigned()->nullable();
            $table->char('image_url')->nullable();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists(self::TABLE_NAME);
    }
}
