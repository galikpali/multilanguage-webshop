<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTagsTable extends Migration
{
    private const TABLE_NAME = 'product_tags';

    private const COLUMN_PRODUCT_ID = 'product_id';
    private const COLUMN_TAG_ID = 'tag_id';

    public function up(): void
    {
        Schema::create(self::TABLE_NAME, function (Blueprint $table): void {
            $table->foreignId(self::COLUMN_PRODUCT_ID)
                ->references(CreateProductsTable::COLUMN_ID)
                ->on(CreateProductsTable::TABLE_NAME)
                ->onDelete('cascade');
            $table->foreignId(self::COLUMN_TAG_ID)
                ->references(CreateTagsTable::COLUMN_ID)
                ->on(CreateTagsTable::TABLE_NAME)
                ->onDelete('cascade');
            $table->primary([
                self::COLUMN_PRODUCT_ID,
                self::COLUMN_TAG_ID,
            ]);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists(self::TABLE_NAME);
    }
}
