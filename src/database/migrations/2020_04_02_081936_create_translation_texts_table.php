<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTranslationTextsTable extends Migration
{
    private const TABLE_NAME = 'translation_texts';

    private const COLUMN_TRANSLATION_ID = 'translation_id';
    private const COLUMN_LANGUAGE_ID = 'language_id';

    public function up(): void
    {
        Schema::create(self::TABLE_NAME, function (Blueprint $table): void {
            $table->foreignId(self::COLUMN_TRANSLATION_ID)
                ->references(CreateTranslationsTable::COLUMN_ID)
                ->on(CreateTranslationsTable::TABLE_NAME);
            $table->foreignId(self::COLUMN_LANGUAGE_ID)
                ->references(CreateLanguagesTable::COLUMN_ID)
                ->on(CreateLanguagesTable::TABLE_NAME)
                ->onDelete('cascade');
            $table->mediumText('text');
            $table->primary([
                self::COLUMN_TRANSLATION_ID,
                self::COLUMN_LANGUAGE_ID,
            ]);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists(self::TABLE_NAME);
    }
}
