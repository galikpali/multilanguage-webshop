<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTagsTable extends Migration
{
    public const TABLE_NAME = 'tags';

    public const COLUMN_ID = 'id';

    public function up(): void
    {
        Schema::create(self::TABLE_NAME, function (Blueprint $table): void {
            $table->id(self::COLUMN_ID);
            $table->foreignId('title_translation_id')
                ->references(CreateTranslationsTable::COLUMN_ID)
                ->on(CreateTranslationsTable::TABLE_NAME);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists(self::TABLE_NAME);
    }
}
