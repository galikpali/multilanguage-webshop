<?php

namespace App\Services;

interface LanguageServiceInterface
{
    public function setCurrentLanguageCode(string $languageCode): void;

    public function getCurrentLanguageCode(): string;

    public function getCurrentLanguageId(): int;
}
