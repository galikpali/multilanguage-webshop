<?php

namespace App\Services;

use App\Repositories\LanguageRepositoryInterface;

class LanguageService implements LanguageServiceInterface
{
    private string $currentLanguage;

    private LanguageRepositoryInterface $languageRepository;

    public function __construct(LanguageRepositoryInterface $languageRepository)
    {
        $this->languageRepository = $languageRepository;
    }

    public function setCurrentLanguageCode(string $languageCode): void
    {
        $this->currentLanguage = $languageCode;
    }

    public function getCurrentLanguageCode(): string
    {
        if (!isset($this->currentLanguage)) {
            return $this->currentLanguage = getenv('DEFAULT_LANGUAGE_CODE');
        }

        return $this->currentLanguage;
    }


    public function getCurrentLanguageId(): int
    {
        $language = $this->languageRepository->findByLanguageCode(
            $this->getCurrentLanguageCode()
        );

        return $language->id();
    }
}
