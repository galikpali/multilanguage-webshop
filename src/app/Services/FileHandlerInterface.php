<?php

namespace App\Services;

use SplFileInfo;

interface FileHandlerInterface
{
    public function uploadFile(SplFileInfo $fileInfo): string;

    public function deleteFileByUrl(string $url): void;
}
