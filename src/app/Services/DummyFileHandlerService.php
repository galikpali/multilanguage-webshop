<?php

namespace App\Services;

use SplFileInfo;

class DummyFileHandlerService implements FileHandlerInterface
{
    public function uploadFile(SplFileInfo $fileInfo): string
    {
        return 'https://somefakeurl.com/image.jpg';
    }

    public function deleteFileByUrl(string $url): void
    {
        return;
    }
}
