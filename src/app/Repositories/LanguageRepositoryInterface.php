<?php

namespace App\Repositories;

use App\Models\LanguageInterface;

interface LanguageRepositoryInterface
{
    public function create(array $attributes): bool;

    /** @return LanguageInterface[] */
    public function findAll(): array;

    public function deleteById(int $id): bool;

    public function findByLanguageCode(string $languageCode): ?LanguageInterface;
}
