<?php

namespace App\Repositories\Eloquent;

use App\Models\Eloquent\Product;
use App\Models\Eloquent\ProductTag;
use App\Models\ProductInterface;
use App\Repositories\Eloquent\Traits\TranslationManagerTrait;
use App\Repositories\ProductRepositoryInterface;
use App\Services\FileHandlerInterface;
use Illuminate\Database\ConnectionInterface;
use Throwable;

class ProductRepository implements ProductRepositoryInterface
{
    use TranslationManagerTrait;

    private ConnectionInterface $connection;

    public function __construct(ConnectionInterface $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param array $attributes
     * @return bool
     * @throws Throwable
     */
    public function create(array $attributes): bool
    {
        $this->connection->beginTransaction();

        $product = new Product();
        $product->title_translation_id = $this->createTranslation($attributes['title']);
        $product->html_description_translation_id = $this->createTranslation($attributes['html_description']);

        $this->setModelBaseAttributes($product, $attributes);

        $product->saveOrFail();

        $this->createTagBindings($product->id, $attributes['tags']);

        $this->connection->commit();

        return true;
    }

    /**
     * @param int $productId
     * @param array $tagIds
     * @throws Throwable
     */
    private function createTagBindings(int $productId, array $tagIds): void
    {
        foreach ($tagIds as $tagId) {
            $productTag = new ProductTag();
            $productTag->product_id = $productId;
            $productTag->tag_id = $tagId;

            $productTag->saveOrFail();
        }
    }

    private function deleteTagBindings(int $productId): void
    {
        ProductTag::where(['product_id' => $productId])->delete();
    }

    public function findAll(): array
    {
        return Product::all()->toArray();
    }

    public function deleteById(int $id): bool
    {
        return Product::destroy($id) > 0;
    }

    public function findById(int $id): ?ProductInterface
    {
        return Product::find($id);
    }

    /**
     * @param int $id
     * @param array $attributes
     * @return bool
     * @throws Throwable
     */
    public function updateById(int $id, array $attributes): bool
    {
        $product = Product::find($id);

        if (!$product) {
            return false;
        }

        $this->connection->beginTransaction();

        if (isset($attributes['title'])) {
            $this->upsertTranslationText(
                $product->title_translation_id,
                $attributes['title']
            );
        }

        if (isset($attributes['html_description'])) {
            $this->upsertTranslationText(
                $product->html_description_translation_id,
                $attributes['html_description']
            );
        }

        $this->setModelBaseAttributes($product, $attributes);

        $product->saveOrFail();

        if (isset($attributes['tags'])) {
            $this->deleteTagBindings($product->id);
            $this->createTagBindings($product->id, $attributes['tags']);
        }

        $this->connection->commit();

        return true;
    }

    private function getFileHandlerService(): FileHandlerInterface
    {
        return app()->get(FileHandlerInterface::class);
    }

    /**
     * @param Product $product
     * @param array $attributes
     * @throws Throwable
     */
    private function setModelBaseAttributes(Product $product, array $attributes): void
    {
        if ($product->image_url) {
            $this->getFileHandlerService()->deleteFileByUrl($product->image_url);
        }

        if (isset($attributes['image'])) {
            $product->image_url = $this->getFileHandlerService()->uploadFile($attributes['image']);
        }

        if (isset($attributes['price'])) {
            $product->price = $attributes['price'];
        }

        if (isset($attributes['published_from'])) {
            $product->published_from = strtotime($attributes['published_from']);
        }

        if (isset($attributes['published_to'])) {
            $product->published_to = strtotime($attributes['published_to']);
        }
    }
}
