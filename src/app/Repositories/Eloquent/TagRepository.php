<?php

namespace App\Repositories\Eloquent;

use App\Models\Eloquent\Tag;
use App\Models\TagInterface;
use App\Repositories\Eloquent\Traits\TranslationManagerTrait;
use App\Repositories\TagRepositoryInterface;
use Illuminate\Database\ConnectionInterface;
use Throwable;

class TagRepository implements TagRepositoryInterface
{
    use TranslationManagerTrait;

    private ConnectionInterface $connection;

    public function __construct(ConnectionInterface $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param array $attributes
     * @return bool
     * @throws Throwable
     */
    public function create(array $attributes): bool
    {
        $this->connection->beginTransaction();

        $tag = new Tag();
        $tag->title_translation_id = $this->createTranslation($attributes['title']);
        $tag->saveOrFail();

        $this->connection->commit();

        return true;
    }

    /** @return Tag[] */
    public function findAll(): array
    {
        return Tag::all()->toArray();
    }

    public function deleteById(int $id): bool
    {
        return Tag::destroy($id) > 0;
    }

    public function findById(int $id): ?TagInterface
    {
        return Tag::find($id);
    }

    /**
     * @param int $id
     * @param array $attributes
     * @return bool
     * @throws Throwable
     */
    public function updateById(int $id, array $attributes): bool
    {
        $tag = Tag::find($id);

        if (!$tag) {
            return false;
        }

        $this->upsertTranslationText($tag->title_translation_id, $attributes['title']);

        return true;
    }
}
