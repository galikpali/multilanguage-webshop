<?php

namespace App\Repositories\Eloquent\Traits;

use App\Models\Eloquent\Translation;
use App\Models\Eloquent\TranslationText;
use App\Services\LanguageServiceInterface;
use Throwable;

trait TranslationManagerTrait
{
    /**
     * @param string $text
     * @return int
     * @throws Throwable
     */
    private function createTranslation(string $text): int
    {
        $translation = new Translation();
        $translation->saveOrFail();

        $translationText = new TranslationText();
        $translationText->translation_id = $translation->id;
        $translationText->language_id = $this->getLanguageService()->getCurrentLanguageId();
        $translationText->text = $text;

        $translationText->saveOrFail();

        return $translation->id;
    }

    /**
     * @param int $translationId
     * @param string $text
     * @throws Throwable
     */
    public function upsertTranslationText(int $translationId, string $text): void
    {
        $currentLanguageId = $this->getLanguageService()->getCurrentLanguageId();

        $translationText = TranslationText::where([
            'translation_id' => $translationId,
            'language_id' => $currentLanguageId,
        ])->first();

        if (!$translationText) {
            $translationText = new TranslationText();
            $translationText->translation_id = $translationId;
            $translationText->language_id = $currentLanguageId;
        }

        $translationText->text = $text;

        $translationText->saveOrFail();
    }

    private function getLanguageService(): LanguageServiceInterface
    {
        return app()->get(LanguageServiceInterface::class);
    }
}
