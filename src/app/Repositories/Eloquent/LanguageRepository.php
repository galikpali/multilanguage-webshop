<?php

namespace App\Repositories\Eloquent;

use App\Models\Eloquent\Language;
use App\Models\LanguageInterface;
use App\Repositories\LanguageRepositoryInterface;

class LanguageRepository implements LanguageRepositoryInterface
{
    public function create(array $attributes): bool
    {
        $model = new Language();
        $model->fill($attributes);
        return $model->save();
    }

    /** @return LanguageInterface[] */
    public function findAll(): array
    {
        return Language::all()->toArray();
    }

    public function deleteById(int $id): bool
    {
        return Language::destroy($id) > 0;
    }

    public function findByLanguageCode(string $languageCode): ?LanguageInterface
    {
        return Language::where(['code' => $languageCode])->first();
    }
}
