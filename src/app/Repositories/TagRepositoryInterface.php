<?php

namespace App\Repositories;

use App\Models\TagInterface;

interface TagRepositoryInterface
{
    public function create(array $attributes): bool;

    /** @return TagInterface[] */
    public function findAll(): array;

    public function deleteById(int $id): bool;

    public function findById(int $id): ?TagInterface;

    public function updateById(int $id, array $attributes): bool;
}
