<?php

namespace App\Repositories;

use App\Models\ProductInterface;

interface ProductRepositoryInterface
{
    public function create(array $attributes): bool;

    /** @return ProductInterface[] */
    public function findAll(): array;

    public function deleteById(int $id): bool;

    public function findById(int $id): ?ProductInterface;

    public function updateById(int $id, array $attributes): bool;
}
