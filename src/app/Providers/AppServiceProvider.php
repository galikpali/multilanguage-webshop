<?php

namespace App\Providers;

use App\Services\DummyFileHandlerService;
use App\Services\FileHandlerInterface;
use App\Services\LanguageService;
use App\Services\LanguageServiceInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->singleton(LanguageServiceInterface::class, LanguageService::class);
        $this->app->singleton(FileHandlerInterface::class, DummyFileHandlerService::class);
    }
}

