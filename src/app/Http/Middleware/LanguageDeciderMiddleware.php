<?php

namespace App\Http\Middleware;

use App\Repositories\LanguageRepositoryInterface;
use App\Services\LanguageServiceInterface;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class LanguageDeciderMiddleware
{
    private LanguageRepositoryInterface $repository;
    private LanguageServiceInterface $languageService;

    public function __construct(
        LanguageRepositoryInterface $repository,
        LanguageServiceInterface $languageService
    )
    {
        $this->repository = $repository;
        $this->languageService = $languageService;
    }

    public function handle(Request $request, Closure $next)
    {
        $language = $request->route('lang');

        if (!$language) {
            return $next($request);
        }

        if (!$this->isLanguageExists($language)) {
            return (new Response())->setStatusCode(Response::HTTP_NOT_FOUND);
        }

        $this->setCurrentLanguageCode($language);

        return $next($request);
    }

    private function isLanguageExists(string $lang): bool
    {
        return (bool)$this->repository->findByLanguageCode($lang);
    }

    private function setCurrentLanguageCode(string $languageCode): void
    {
        $this->languageService->setCurrentLanguageCode($languageCode);
    }
}
