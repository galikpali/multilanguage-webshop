<?php

namespace App\Http\Controllers;

use App\Repositories\ProductRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Routing\Controller;

class ProductController extends Controller
{
    private ProductRepositoryInterface $repository;

    public function __construct(ProductRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @return Response
     * @throws ValidationException
     */
    public function create(Request $request): Response
    {
        $this->validate($request, $this->getCreateValidationRules());

        $createSuccess = $this->repository->create($request->all());

        $statusCode = $createSuccess
            ? Response::HTTP_CREATED
            : Response::HTTP_INTERNAL_SERVER_ERROR;

        return (new Response())->setStatusCode($statusCode);
    }

    public function listAll(): Response
    {
        return (new Response())->setContent($this->repository->findAll());
    }

    public function delete(int $id): Response
    {
        $statusCode = $this->repository->deleteById($id)
            ? Response::HTTP_NO_CONTENT
            : Response::HTTP_NOT_FOUND;

        return (new Response())->setStatusCode($statusCode);
    }

    public function read(int $id): Response
    {
        $model = $this->repository->findById($id);

        if (!$model) {
            return (new Response())->setStatusCode(Response::HTTP_NOT_FOUND);
        }

        return (new Response())->setContent($model);
    }

    /**
     * @param int $id
     * @param Request $request
     * @return Response
     * @throws ValidationException
     */
    public function patch(int $id, Request $request): Response
    {
        $this->validate($request, $this->getPatchValidationRules());

        $statusCode = $this->repository->updateById($id, $request->all())
            ? Response::HTTP_NO_CONTENT
            : Response::HTTP_NOT_FOUND;

        return (new Response())->setStatusCode($statusCode);
    }

    private function getCreateValidationRules(): array
    {
        return [
            'title' => 'required|string|max:255',
            'html_description' => 'required|string',
            'tags' => 'required|array|exists:tags,id',
            'published_from' => 'required|date',
            'published_to' => 'date|after_or_equal:published_from',
            'price' => 'required|integer',
            'image' => 'image|mimes:jpg,jpeg,png,gif',
        ];
    }

    private function getPatchValidationRules(): array
    {
        return [
            'title' => 'string|max:255',
            'html_description' => 'string',
            'tags' => 'array|exists:tags,id',
            'published_from' => 'date',
            'published_to' => 'date|after_or_equal:published_from',
            'price' => 'integer',
            'image' => 'image|mimes:jpg,jpeg,png,gif',
        ];
    }
}
