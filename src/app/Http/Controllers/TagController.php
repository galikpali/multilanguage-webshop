<?php

namespace App\Http\Controllers;

use App\Repositories\TagRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Routing\Controller;

class TagController extends Controller
{
    private const VALIDATION_RULE_TITLE = 'required|string|max:255';

    private TagRepositoryInterface $repository;

    public function __construct(TagRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @return Response
     * @throws ValidationException
     */
    public function create(Request $request): Response
    {
        $this->validate($request, ['title' => self::VALIDATION_RULE_TITLE]);

        $statusCode = $this->repository->create(['title' => $request->get('title')])
            ? Response::HTTP_CREATED
            : Response::HTTP_INTERNAL_SERVER_ERROR;

        return (new Response())->setStatusCode($statusCode);
    }

    public function listAll(): Response
    {
        return (new Response())->setContent($this->repository->findAll());
    }

    public function delete(int $id): Response
    {
        $statusCode = $this->repository->deleteById($id)
            ? Response::HTTP_NO_CONTENT
            : Response::HTTP_NOT_FOUND;

        return (new Response())->setStatusCode($statusCode);
    }

    public function read(int $id): Response
    {
        $model = $this->repository->findById($id);

        if (!$model) {
            return (new Response())->setStatusCode(Response::HTTP_NOT_FOUND);
        }

        return (new Response())->setContent($model);
    }

    /**
     * @param int $id
     * @param Request $request
     * @return Response
     * @throws ValidationException
     */
    public function patch(int $id, Request $request): Response
    {
        $this->validate($request, ['title' => self::VALIDATION_RULE_TITLE]);

        $statusCode = $this->repository->updateById($id, ['title' => $request->get('title')])
            ? Response::HTTP_NO_CONTENT
            : Response::HTTP_NOT_FOUND;

        return (new Response())->setStatusCode($statusCode);
    }
}
