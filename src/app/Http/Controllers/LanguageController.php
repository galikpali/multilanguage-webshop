<?php

namespace App\Http\Controllers;

use App\Repositories\LanguageRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Routing\Controller;

class LanguageController extends Controller
{
    private LanguageRepositoryInterface $repository;

    public function __construct(LanguageRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @return Response
     * @throws ValidationException
     */
    public function create(Request $request): Response
    {
        $this->validate($request, ['code' => 'required|string|unique:languages|max:2']);

        $createSuccess = $this->repository->create($request->all());

        $statusCode = $createSuccess
            ? Response::HTTP_CREATED
            : Response::HTTP_INTERNAL_SERVER_ERROR;

        return (new Response())->setStatusCode($statusCode);
    }

    public function listAll(): Response
    {
        return (new Response())->setContent($this->repository->findAll());
    }

    public function delete(int $id): Response
    {
        $statusCode = $this->repository->deleteById($id)
            ? Response::HTTP_NO_CONTENT
            : Response::HTTP_NOT_FOUND;

        return (new Response())->setStatusCode($statusCode);
    }
}
