<?php

namespace App\Models;

use Illuminate\Contracts\Support\Jsonable;

interface TagInterface extends Jsonable
{
    public function id(): int;

    public function title(): ?string;
}
