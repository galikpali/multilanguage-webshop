<?php

namespace App\Models;

use Illuminate\Contracts\Support\Jsonable;

interface ProductInterface extends Jsonable
{
    public function id(): int;

    public function title(): ?string;

    public function htmlDescription(): ?string;

    /** @return TagInterface[] */
    public function tags(): array;

    public function getImageUrl(): ?string;

    public function publishedFrom(): ?int;

    public function publishedTo(): ?int;

    public function price(): int;
}
