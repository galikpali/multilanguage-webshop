<?php

namespace App\Models\Eloquent;

use App\Models\Eloquent\Traits\TranslationFinderTrait;
use App\Models\ProductInterface;
use App\Models\TagInterface;
use DateTime;
use Exception;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Eloquent\Product
 *
 * @property int $id
 * @property int $title_translation_id
 * @property int $html_description_translation_id
 * @property int $price
 * @property int|null $published_from
 * @property int|null $published_to
 * @property string $image_url
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eloquent\Product newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eloquent\Product newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eloquent\Product query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eloquent\Product whereHtmlDescriptionTranslationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eloquent\Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eloquent\Product whereImageUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eloquent\Product wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eloquent\Product wherePublishedFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eloquent\Product wherePublishedTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eloquent\Product whereTitleTranslationId($value)
 * @mixin \Eloquent
 */
class Product extends Model implements ProductInterface
{
    use TranslationFinderTrait;

    public $timestamps = false;

    protected $table = 'products';

    public function id(): int
    {
        return $this->id;
    }

    public function title(): ?string
    {
        return $this->findTranslationByKey('title_translation_id');
    }

    public function htmlDescription(): ?string
    {
        return $this->findTranslationByKey('html_description_translation_id');
    }

    /** @return TagInterface[] */
    public function tags(): array
    {
        return $this->hasManyThrough(
            Tag::class,
            ProductTag::class,
            'product_id',
            'id',
            'id',
            'tag_id'
        )->get()->all();
    }

    public function getImageUrl(): ?string
    {
        return $this->image_url;
    }

    public function publishedFrom(): ?int
    {
        return $this->published_from;
    }

    public function publishedTo(): ?int
    {
        return $this->published_to;
    }

    public function price(): int
    {
        return $this->price;
    }

    /**
     * @param int $options
     * @return array
     * @throws Exception
     */
    public function toArray(int $options = 0): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title(),
            'html_description' => $this->htmlDescription(),
            'tags' => $this->tags(),
            'published_from' => $this->createDateFormatFromTimestamp($this->published_from),
            'published_to' => $this->createDateFormatFromTimestamp($this->published_to),
            'image_url' => $this->image_url,
            'price' => $this->price,
        ];
    }

    /**
     * @param int $timestamp
     * @return string
     * @throws Exception
     */
    private function createDateFormatFromTimestamp(?int $timestamp): ?string
    {
        if (!$timestamp) {
            return null;
        }

        return (new DateTime())
            ->setTimestamp($timestamp)
            ->format('Y-m-d');
    }
}
