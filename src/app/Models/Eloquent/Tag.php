<?php

namespace App\Models\Eloquent;

use App\Models\Eloquent\Traits\TranslationFinderTrait;
use App\Models\TagInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Eloquent\Tag
 *
 * @property int $id
 * @property int $title_translation_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eloquent\Tag newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eloquent\Tag newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eloquent\Tag query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eloquent\Tag whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eloquent\Tag whereTitleTranslationId($value)
 * @mixin \Eloquent
 */
class Tag extends Model implements TagInterface
{
    use TranslationFinderTrait;

    public $timestamps = false;

    protected $table = 'tags';

    public function id(): int
    {
        return $this->id;
    }

    public function title(): ?string
    {
        return $this->findTranslationByKey('title_translation_id');
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title(),
        ];
    }
}
