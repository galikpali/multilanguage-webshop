<?php

namespace App\Models\Eloquent;

use App\Models\LanguageInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Eloquent\Language
 *
 * @property int $id
 * @property string $code
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eloquent\Language newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eloquent\Language newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eloquent\Language query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eloquent\Language whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eloquent\Language whereId($value)
 * @mixin \Eloquent
 */
class Language extends Model implements LanguageInterface
{
    public $timestamps = false;

    protected $table = 'languages';

    protected $fillable = [
        'code',
    ];

    public function id(): int
    {
        return $this->id;
    }

    public function code(): string
    {
        return $this->code;
    }
}
