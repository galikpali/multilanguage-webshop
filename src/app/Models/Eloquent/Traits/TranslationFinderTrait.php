<?php

namespace App\Models\Eloquent\Traits;

use App\Models\Eloquent\Translation;
use App\Models\Eloquent\TranslationText;
use App\Services\LanguageServiceInterface;

trait TranslationFinderTrait
{
    private function findTranslationByKey(string $key): ?string
    {
        $relation = $this->hasOneThrough(
            TranslationText::class,
            Translation::class,
            'id',
            'translation_id',
            $key
        )
            ->join('languages', 'translation_texts.language_id', '=', 'languages.id')
            ->where('languages.code', '=', $this->getLanguageService()->getCurrentLanguageCode());

        return $relation->first()->text ?? null;
    }

    private function getLanguageService(): LanguageServiceInterface
    {
        return app()->get(LanguageServiceInterface::class);
    }
}
