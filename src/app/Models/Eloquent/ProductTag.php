<?php

namespace App\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Eloquent\ProductTag
 *
 * @property int $product_id
 * @property int $tag_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eloquent\ProductTag newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eloquent\ProductTag newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eloquent\ProductTag query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eloquent\ProductTag whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eloquent\ProductTag whereTagId($value)
 * @mixin \Eloquent
 */
class ProductTag extends Model
{
    public $timestamps = false;

    protected $table = 'product_tags';
}
