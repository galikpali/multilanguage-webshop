<?php

namespace App\Models\Eloquent;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Eloquent\TranslationText
 *
 * @property int $translation_id
 * @property int $language_id
 * @property string $text
 * @method static Builder|\App\Models\Eloquent\TranslationText newModelQuery()
 * @method static Builder|\App\Models\Eloquent\TranslationText newQuery()
 * @method static Builder|\App\Models\Eloquent\TranslationText query()
 * @method static Builder|\App\Models\Eloquent\TranslationText whereLanguageId($value)
 * @method static Builder|\App\Models\Eloquent\TranslationText whereText($value)
 * @method static Builder|\App\Models\Eloquent\TranslationText whereTranslationId($value)
 * @mixin \Eloquent
 */
class TranslationText extends Model
{
    public $timestamps = false;

    public $incrementing = false;

    protected $table = 'translation_texts';

    protected $primaryKey = [
        'translation_id',
        'language_id',
    ];

    protected function setKeysForSaveQuery(Builder $query): Builder
    {
        $keys = $this->getKeyName();
        if (!is_array($keys)) {
            return parent::setKeysForSaveQuery($query);
        }

        foreach ($keys as $keyName) {
            $query->where($keyName, '=', $this->getKeyForSaveQuery($keyName));
        }

        return $query;
    }

    protected function getKeyForSaveQuery($keyName = null)
    {
        if (is_null($keyName)) {
            $keyName = $this->getKeyName();
        }

        if (isset($this->original[$keyName])) {
            return $this->original[$keyName];
        }

        return $this->getAttribute($keyName);
    }
}
