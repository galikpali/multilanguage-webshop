<?php

namespace App\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Eloquent\Translation
 *
 * @property int $id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eloquent\Translation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eloquent\Translation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eloquent\Translation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eloquent\Translation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eloquent\Translation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eloquent\Translation whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Translation extends Model
{
    public $timestamps = false;

    protected $table = 'translations';
}
