<?php

namespace App\Models;

use Illuminate\Contracts\Support\Jsonable;

interface LanguageInterface extends Jsonable
{
    public function id(): int;

    public function code(): string;
}
