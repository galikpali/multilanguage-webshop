<?php

use App\Http\Middleware\LanguageDeciderMiddleware;
use App\Repositories\Eloquent\LanguageRepository;
use App\Repositories\Eloquent\ProductRepository;
use App\Repositories\Eloquent\TagRepository;
use App\Repositories\LanguageRepositoryInterface;
use App\Repositories\ProductRepositoryInterface;
use App\Repositories\TagRepositoryInterface;
use Illuminate\Database\ConnectionInterface;

require_once __DIR__ . '/../vendor/autoload.php';

(new Laravel\Lumen\Bootstrap\LoadEnvironmentVariables(
    dirname(__DIR__)
))->bootstrap();

date_default_timezone_set(env('APP_TIMEZONE', 'UTC'));

$app = new Laravel\Lumen\Application(
    dirname(__DIR__)
);

$app->withFacades();
$app->withEloquent();

$app->bind(LanguageRepositoryInterface::class, LanguageRepository::class);
$app->bind(ProductRepositoryInterface::class, ProductRepository::class);
$app->bind(TagRepositoryInterface::class, TagRepository::class);

$app->bind(ConnectionInterface::class, fn() => DB::connection());

$app->singleton(Illuminate\Contracts\Debug\ExceptionHandler::class, App\Exceptions\Handler::class);
$app->singleton(Illuminate\Contracts\Console\Kernel::class, App\Console\Kernel::class);

$app->configure('app');

$app->routeMiddleware(['languageDecider' => LanguageDeciderMiddleware::class]);

$app->register(App\Providers\AppServiceProvider::class);

$app->middleware([
    App\Http\Middleware\CorsMiddleware::class
]);

$app->router->group([
    'namespace' => 'App\Http\Controllers',
], function ($router) {
    require __DIR__ . '/../routes/web.php';
});

return $app;
