<?php

use Laravel\Lumen\Routing\Router;

/** @var Router $router */
$router->post('/language', 'LanguageController@create');
$router->get('/languages', 'LanguageController@listAll');
$router->delete('/language/{id:\d*}', 'LanguageController@delete');

$router->group(['middleware' => 'languageDecider'], function () use ($router): void {
    $languagePattern = '[/{lang:[a-z]{2}}]';

    $router->post("/product{$languagePattern}", 'ProductController@create');
    $router->get("product/{id:\d*}{$languagePattern}", 'ProductController@read');
    $router->patch("/product/{id:\d*}{$languagePattern}", 'ProductController@patch');
    $router->get("/products{$languagePattern}", 'ProductController@listAll');

    $router->post("/tag{$languagePattern}", 'TagController@create');
    $router->get("/tag/{id:\d*}{$languagePattern}", 'TagController@read');
    $router->patch("/tag/{id:\d*}{$languagePattern}", 'TagController@patch');
    $router->get("/tags{$languagePattern}", 'TagController@listAll');
});

$router->delete('/product/{id:\d*}', 'ProductController@delete');
$router->delete('/tag/{id:\d*}', 'TagController@delete');
