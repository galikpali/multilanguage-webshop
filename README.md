# Multilanguage Webshop REST API

## How to launch project in dev mode
1. Make sure you have a running **docker** daemon on your machine
2. The docker engine must be **version 18.06** or higher in order to launch the project
3. Make sure **port 8080, 8090 and 3306 is not taken**
4. Launch the services by the following command:
    ```
        make dev
    ```
    
5. At first launch it can take **few minutes to start** because of the build process   
6. After the project successfully launched:
    * http://localhost:8090 - Swagger UI based on the API [openapi documentation](./api/openapi.yaml).
     Here you you can try all types of requests for each resources.
    * http://localhost:8080 - The API itself
    
## REST API documentation
The API documentation can be found [here](./api/openapi.yaml)